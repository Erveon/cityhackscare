import Vue from 'vue'
import Router from 'vue-router'

import LoginPage from '@/components/LoginPage'
import SignupPage from '@/components/SignupPage'

import VolunteerPage from '@/components/volunteers/VolunteerPage'
import CaretakerPage from '@/components/caretakers/CaretakerPage'

import VolunteerActive from '@/components/volunteers/VolunteerActive'
import VolunteerContacts from '@/components/volunteers/VolunteerContacts'
import VolunteerProfile from '@/components/volunteers/VolunteerProfile'

import CaretakerPeople from '@/components/caretakers/CaretakerPeople'
import CaretakerHistory from '@/components/caretakers/CaretakerHistory'
import CaretakerVolunteers from '@/components/caretakers/CaretakerVolunteers'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'LoginPage', component: LoginPage },
    { path: '/signup/:type', name: 'SignupPage', component: SignupPage },
    { path: '/volunteer', component: VolunteerPage,
      children: [
        { path: '', redirect: { name: 'VolunteerActive' } },
        { path: '/volunteer/active', name: 'VolunteerActive', component: VolunteerActive },
        { path: '/volunteer/contacts', name: 'VolunteerContacts', component: VolunteerContacts },
        { path: '/volunteer/profile', name: 'VolunteerProfile', component: VolunteerProfile }
      ]
    },
    { path: '/caretaker', component: CaretakerPage,
      children: [
        { path: '', redirect: { name: 'CaretakerPeople' } },
        { path: '/caretaker/people', name: 'CaretakerPeople', component: CaretakerPeople },
        { path: '/caretaker/history', name: 'CaretakerHistory', component: CaretakerHistory },
        { path: '/caretaker/volunteers', name: 'CaretakerVolunteers', component: CaretakerVolunteers }
      ] 
    }
  ]
})
